const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')

module.exports = (_env, args) => ({
    mode: args.mode || 'development',
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                path.resolve(__dirname, './src/injector.js'),
                {
                    from: path.resolve(__dirname, './src/manifest.json'),
                    transform: (content) => Buffer.from(
                        JSON.stringify(
                            {
                                description: process.env.npm_package_description,
                                version: process.env.npm_package_version,
                                ...JSON.parse(content.toString())
                            },
                            null,
                            4
                        ),
                    )
                },
            ]
        })
    ]
})
