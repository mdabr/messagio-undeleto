import { MESSEGIO_UNDELETO_ID_NAME } from './constants'
import { createDb } from './db'
import {
  createGridObserver,
  filterTaggedTabElementsOut,
  get,
  getAll,
  getGridElement,
  getMessageElements,
  getTabElements,
  idFromElement,
  lengthGreaterThan,
  lengthGreaterThan0,
  nodeListToArray,
  querySelectorAllWithParent,
  tagTabElement,
} from './helpers'
import { createMessage } from './message'

describe('helpers', () => {
  let body: string = null
  let mutationCallback: MutationCallback = null
  const globalMutationObserver = global.MutationObserver

  beforeEach(() => {
    body = document.body.innerHTML

    /* eslint-disable */
      // @ts-ignore
      global.MutationObserver = class {
        // @ts-ignore
        constructor(callback) {
          mutationCallback = callback
        }
        
        // @ts-ignore
        observe(element, initObject) {}
        /* eslint-enable */
    }
  })

  afterEach(() => {
    document.body.innerHTML = body

    global.MutationObserver = globalMutationObserver
  })

  describe('getMessageElements', () => {
    it('returns empty array when no message elements were found', () => {
      const gridElement = document.createElement('div')

      document.body.appendChild(gridElement)

      const messageElements = getMessageElements(gridElement)

      expect(messageElements.length).toEqual(0)
    })

    it('returns message elements', () => {
      const gridElement = document.createElement('div')
      const messageElement1 = document.createElement('div')
      const messageElement2 = document.createElement('div')

      messageElement1.dataset.testid = 'message-container'
      messageElement2.dataset.testid = 'message-container'

      gridElement.appendChild(messageElement1)
      gridElement.appendChild(messageElement2)
      document.body.appendChild(gridElement)

      const messageElements = getMessageElements(gridElement)

      expect(messageElements.length).toEqual(2)
      expect(messageElements[0]).toEqual(messageElement1)
      expect(messageElements[1]).toEqual(messageElement2)
    })
  })

  describe('getGridElement', () => {
    it('returns body when grid element was not found in the tab element', () => {
      const tabElement = document.createElement('div')
      document.body.appendChild(tabElement)

      const gridElement = getGridElement(tabElement)

      expect(gridElement).toEqual(document.body)
    })

    it('returns grid element found in the tab element', () => {
      const tabElement = document.createElement('div')
      document.body.appendChild(tabElement)
      const gridElement = document.createElement('div')
      gridElement.setAttribute('role', 'grid')
      tabElement.appendChild(gridElement)

      const foundGridElement = getGridElement(tabElement)

      expect(foundGridElement).not.toEqual(document.body)
      expect(foundGridElement instanceof HTMLElement).toEqual(true)
    })
  })

  describe('idFromElement', () => {
    it('returns ID found in element', () => {
      const element = document.createElement('div')
      element.dataset[MESSEGIO_UNDELETO_ID_NAME] = 'test-id'

      const id = idFromElement(element)

      expect(id).toEqual('test-id')
    })

    it('returns undefind when ID was not found in element', () => {
      const element = document.createElement('div')

      const id = idFromElement(element)

      expect(id).toEqual(undefined)
    })
  })

  describe('get', () => {
    it('returns element found in document based on selector', () => {
      const selector = 'div[data-test="test"]'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      document.body.appendChild(element)

      const foundElement = get(selector)

      expect(foundElement.dataset.test).toEqual('test')
    })

    it('returns element found in parent element passed based on selector', () => {
      const selector = 'div[data-test="test"]'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      const parentElement = document.createElement('header')
      parentElement.appendChild(element)
      document.body.appendChild(parentElement)

      const foundElement = get(selector, parentElement)

      expect(foundElement.dataset.test).toEqual('test')
    })

    it('returns body when element was not found in document based on selector', () => {
      const selector = 'notWorkingSelector'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      document.body.appendChild(element)

      const foundElement = get(selector)

      expect(foundElement).toEqual(document.body)
    })

    it('returns body when element was not found in parent element based on selector', () => {
      const selector = 'notWorkingSelector'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      const parentElement = document.createElement('header')
      parentElement.appendChild(element)
      document.body.appendChild(parentElement)

      const foundElement = get(selector, parentElement)

      expect(foundElement).toEqual(document.body)
    })
  })

  describe('getAll', () => {
    it('returns element found in document based on selector', () => {
      const selector = 'div[data-test="test"]'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      document.body.appendChild(element)

      const [foundElement] = getAll(selector)

      expect(foundElement.dataset.test).toEqual('test')
    })

    it('returns element found in parent element passed based on selector', () => {
      const selector = 'div[data-test="test"]'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      const parentElement = document.createElement('header')
      parentElement.appendChild(element)
      document.body.appendChild(parentElement)

      const [foundElement] = getAll(selector, parentElement)

      expect(foundElement.dataset.test).toEqual('test')
    })

    it('returns empty array when element was not found in document based on selector', () => {
      const selector = 'notWorkingSelector'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      document.body.appendChild(element)

      const foundElements = getAll(selector)

      expect(foundElements).toEqual([])
    })

    it('returns empty array when element was not found in parent element based on selector', () => {
      const selector = 'notWorkingSelector'
      const element = document.createElement('div')
      element.dataset.test = 'test'
      const parentElement = document.createElement('header')
      parentElement.appendChild(element)
      document.body.appendChild(parentElement)

      const foundElements = getAll(selector, parentElement)

      expect(foundElements).toEqual([])
    })
  })

  describe('lengthGreaterThan', () => {
    it('returns true when length is greater than passed number', () => {
      const result = lengthGreaterThan(1)({ length: 2 })

      expect(result).toEqual(true)
    })

    it('returns false when length is equal to passed number', () => {
      const result = lengthGreaterThan(0)({ length: 0 })

      expect(result).toEqual(false)
    })

    it('returns false when length is lower than passed number', () => {
      const result = lengthGreaterThan(1)({ length: 0 })

      expect(result).toEqual(false)
    })
  })

  describe('lengthGreaterThan0', () => {
    it('returns true when length is greater than 0', () => {
      const result = lengthGreaterThan0({ length: 2 })

      expect(result).toEqual(true)
    })

    it('returns false when length is equal to 0', () => {
      const result = lengthGreaterThan0({ length: 0 })

      expect(result).toEqual(false)
    })

    it('returns false when length is lower than 0', () => {
      const result = lengthGreaterThan0({ length: -1 })

      expect(result).toEqual(false)
    })
  })

  describe('nodeListToArray', () => {
    it('returns an array with elements passed in node list', () => {
      document.body.append(document.createElement('div'))
      document.body.append(document.createElement('div'))
      document.body.append(document.createElement('div'))
      const nodeList = document.querySelectorAll('div')

      const array = nodeListToArray(nodeList)

      expect(array).toEqual([
        expect.any(HTMLElement),
        expect.any(HTMLElement),
        expect.any(HTMLElement),
      ])
    })
  })

  describe('querySelectorAllWithParent', () => {
    it('returns elements based on selector when parent is undefined', () => {
      document.body.append(document.createElement('div'))

      const elements = querySelectorAllWithParent()('div')

      expect(elements[0]).toEqual(expect.any(HTMLElement))
    })

    it('returns elements based on selector when parent is defined', () => {
      const parentElement = document.createElement('div')
      parentElement.appendChild(document.createElement('div'))
      document.body.append(parentElement)

      const elements = querySelectorAllWithParent(parentElement)('div')

      expect(elements[0]).toEqual(expect.any(HTMLElement))
    })
  })

  describe('createGridObserver', () => {
    it('returns a new mutation observer designed to react to grid changes', () => {
      const db = createDb()
      const gridElement = document.createElement('div')
      document.body.appendChild(gridElement)

      const observer = createGridObserver(db)(gridElement)

      expect(observer).toEqual(expect.any(MutationObserver))
    })

    it('returned observer reacts to changes in grid', () => {
      const db = createDb()
      const gridElement = document.createElement('div')
      const mockMessageElement = document.createElement('div')
      mockMessageElement.dataset.testid = 'message-container'
      gridElement.appendChild(mockMessageElement)
      document.body.appendChild(gridElement)
      const firstMessage = db.addMessage(createMessage(mockMessageElement))

      const observer = createGridObserver(db)(gridElement)
      observer.observe(gridElement, { childList: true, subtree: true })
      const messageElement = document.createElement('div')
      messageElement.dataset.testid = 'message-container'
      gridElement.appendChild(messageElement)

      mutationCallback([], observer)

      expect(db.getMessages().map((el) => el.getId())).toEqual([
        firstMessage.getId(),
        messageElement.dataset[MESSEGIO_UNDELETO_ID_NAME],
      ])
    })
  })

  describe('getTabElements', () => {
    it('returns found tab elements', () => {
      const tabElementParent = document.createElement('div')
      tabElementParent.dataset.testid = 'mwchat-tabs'
      const tabElement = document.createElement('div')
      tabElement.dataset.testid = 'hello'
      tabElementParent.appendChild(tabElement)
      document.body.appendChild(tabElementParent)

      const foundTabElements = getTabElements()

      expect(foundTabElements).toEqual([tabElement])
    })
  })

  describe('filterTaggedTabElementsOut', () => {
    it('filters out the tab elements that already have an ID', () => {
      const tabElement1 = document.createElement('div')
      tabElement1.dataset[MESSEGIO_UNDELETO_ID_NAME] = 'test-id'
      const tabElement2 = document.createElement('div')

      const filteredElements = filterTaggedTabElementsOut([
        tabElement1,
        tabElement2,
      ])

      expect(filteredElements).toEqual([tabElement2])
    })
  })

  describe('tagTabElement', () => {
    it('adds ID to each tab element', () => {
      const tabElement1 = document.createElement('div')

      const taggedTabElements = tagTabElement([tabElement1])

      expect(taggedTabElements[0].dataset[MESSEGIO_UNDELETO_ID_NAME]).toEqual(
        expect.any(String)
      )
    })
  })
})
