export const MESSEGIO_UNDELETO_ID_NAME = 'messegioUndeletoId'
export const MESSAGE_ELEMENT_SELECTOR = '[data-testid="message-container"]'
export const MESSAGE_CHILDREN_CONTAINER_SELECTOR =
  'div:first-child > div:first-child > div:nth-child(2) > span > div:nth-child(2) > div:nth-child(2) > div > div'
export const DELETED_MESSAGE_SELECTOR = 'div:first-child > div > span > div'
export const GRID_ELEMENT_SELECTOR = '[role=grid]'
export const REMOVED_MESSAGE_SELECTOR = 'span>div'
export const TAB_ELEMENT_SELECTOR = '[data-testid=mwchat-tabs]>div'
