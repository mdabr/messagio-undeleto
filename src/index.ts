import { createDb, DbController } from './db'
import {
  createGridObserver,
  filterTaggedTabElementsOut,
  getGridElement,
  getMessageElements,
  getTabElements,
  tagTabElement,
} from './helpers'
import { createMessage } from './message'
import { pipe, map, forEach } from 'ramda'

const setupTab = (tabElement: HTMLElement) => (db: DbController) =>
  pipe(
    getGridElement,
    getMessageElements,
    map(createMessage),
    map(db.addMessage),
    () => db
  )(tabElement)

const setupObserver = (tabElement: HTMLElement) => (db: DbController) =>
  pipe(getGridElement, createGridObserver(db), (observer: MutationObserver) =>
    observer.observe(getGridElement(tabElement), {
      childList: true,
      subtree: true,
    })
  )(tabElement)

// Init
setInterval(
  pipe(
    getTabElements,
    filterTaggedTabElementsOut,
    tagTabElement,
    forEach((tabElement: HTMLElement) =>
      pipe(createDb, setupTab(tabElement), setupObserver(tabElement))()
    )
  ),
  5000
)
