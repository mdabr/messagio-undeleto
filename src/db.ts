import {
  DELETED_MESSAGE_SELECTOR,
  MESSAGE_CHILDREN_CONTAINER_SELECTOR,
  MESSEGIO_UNDELETO_ID_NAME,
  REMOVED_MESSAGE_SELECTOR,
} from './constants'
import { createMessage, Message } from './message'

export interface DbController {
  addMessage: (message: Message) => Message
  removeTrash: (ids: string[]) => boolean
  addOrUpdateMessage: (messageElement: HTMLElement) => void
  getMessages: () => Message[]
}

export function createDb(): DbController {
  const db: Map<string, Message> = new Map()
  const updateMessage = (messageId: string, newMessage: string): void => {
    const message = db.get(messageId)

    message.setText(newMessage)
  }
  // When element is not in DOM anymore but is still stored in database
  const isTrash =
    (ids: string[]) =>
    (messageId: string): boolean =>
      !ids.includes(messageId)

  const dbController: DbController = {
    addMessage(message: Message): Message {
      if (message === null) {
        return null
      }

      db.set(message.getId(), message)

      return message
    },

    removeTrash(ids: string[]): boolean {
      return Array.from(db.keys())
        .filter(isTrash(ids))
        .map((id) => db.delete(id))
        .includes(false)
    },

    addOrUpdateMessage(messageElement: HTMLElement) {
      const messageDatasetId = messageElement.dataset[MESSEGIO_UNDELETO_ID_NAME]

      if (messageDatasetId) {
        const previousMessage = db.get(messageDatasetId)

        if (previousMessage) {
          const messageChildren = messageElement.querySelector(
            MESSAGE_CHILDREN_CONTAINER_SELECTOR
          )

          if (!messageChildren) {
            const messageDeleted = messageElement.querySelector(
              DELETED_MESSAGE_SELECTOR
            )
            const tempElement = document.createElement('div')
            tempElement.innerHTML = previousMessage.getText()

            if (tempElement.textContent !== messageDeleted.textContent) {
              const element = messageElement.querySelector(
                REMOVED_MESSAGE_SELECTOR
              )

              if (element) {
                element.innerHTML += ` ${previousMessage.getText()}`

                updateMessage(messageDatasetId, element.innerHTML)
              }
            }
          }
        }
      } else {
        dbController.addMessage(createMessage(messageElement))
      }
    },

    getMessages() {
      return Array.from(db.values())
    },
  }

  return dbController
}
