import { MESSEGIO_UNDELETO_ID_NAME } from './constants'
import { createDb } from './db'
import { createMessage } from './message'

const createMessageElement = (message: string) => {
  const messageElement = document.createElement('div')
  messageElement.innerHTML = message
  document.body.appendChild(messageElement)

  return messageElement
}

describe('db', () => {
  let body: string = null

  beforeEach(() => {
    body = document.body.innerHTML
  })

  afterEach(() => {
    document.body.innerHTML = body
  })

  describe('createDb', () => {
    describe('addMessage', () => {
      it('adds message to db and returns that message', () => {
        const db = createDb()
        const messageElement = createMessageElement('test')

        const message = db.addMessage(createMessage(messageElement))

        expect(db.getMessages()).toEqual([message])
      })

      it("does not add message to db if it's null and returns it", () => {
        const db = createDb()

        const message = db.addMessage(null)

        expect(db.getMessages()).toEqual([])
        expect(message).toEqual(null)
      })
    })

    describe('removeTrash', () => {
      it('removes elements that are not present in provided array', () => {
        const db = createDb()
        const messageElement = createMessageElement('test')
        db.addMessage(createMessage(messageElement))

        db.removeTrash(['some-id-that-does-not-exist'])

        expect(db.getMessages()).toEqual([])
      })

      it('does not remove elements that are present in provided array', () => {
        const db = createDb()
        const messageElement = createMessageElement('test')
        const message = db.addMessage(createMessage(messageElement))

        db.removeTrash([message.getId()])

        expect(db.getMessages()).toEqual([message])
      })
    })

    describe('addOrUpdateMessage', () => {
      it('adds a message if message element was not tagged yet', () => {
        const db = createDb()
        const messageElement = createMessageElement('test')

        db.addOrUpdateMessage(messageElement)

        expect(db.getMessages()[0].getElement()).toEqual(messageElement)
      })

      it('updates message if message element was tagged', () => {
        const db = createDb()
        const messageElement = createMessageElement('test')

        db.addOrUpdateMessage(messageElement)
        messageElement.innerHTML = '<span><div>Deleted message</div></span>'

        db.addOrUpdateMessage(messageElement)

        expect(db.getMessages()[0].getText()).toEqual('Deleted message (test)')
      })

      it('does not update message if message element was tagged with incorrect ID', () => {
        const db = createDb()
        const messageElement = createMessageElement('test')

        db.addOrUpdateMessage(messageElement)
        messageElement.innerHTML = '<span><div>Deleted message</div></span>'
        messageElement.dataset[MESSEGIO_UNDELETO_ID_NAME] = 'incorrect-id'

        db.addOrUpdateMessage(messageElement)

        expect(db.getMessages()[0].getText()).toEqual('test')
      })
    })

    describe('getMessages', () => {
      it('returns messages cached in db', () => {
        const db = createDb()
        const messageElement = createMessageElement('test')
        const message = db.addMessage(createMessage(messageElement))

        expect(db.getMessages()).toEqual([message])
      })
    })
  })
})
