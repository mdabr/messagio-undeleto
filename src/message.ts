import { v4 } from 'uuid'
import {
  DELETED_MESSAGE_SELECTOR,
  MESSAGE_CHILDREN_CONTAINER_SELECTOR,
  MESSEGIO_UNDELETO_ID_NAME,
} from './constants'

export interface Message {
  setText(newText: string): void
  getText(): string
  getElement(): HTMLElement
  getId(): string
}

export function createMessage(messageElement: HTMLElement): Message {
  let message = (
    messageElement.querySelector(MESSAGE_CHILDREN_CONTAINER_SELECTOR) ||
    messageElement.querySelector(DELETED_MESSAGE_SELECTOR)
  ).innerHTML
  const id = v4()

  messageElement.dataset[MESSEGIO_UNDELETO_ID_NAME] = id

  return {
    setText(newText: string): void {
      message = newText
    },
    getText(): string {
      return message
    },
    getElement(): HTMLElement {
      return messageElement
    },
    getId(): string {
      return id
    },
  }
}
