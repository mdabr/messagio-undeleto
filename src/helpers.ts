import { WithLength } from './types'
import { pipe, ifElse, always, filter, map } from 'ramda'
import { DbController } from './db'
import {
  GRID_ELEMENT_SELECTOR,
  MESSAGE_ELEMENT_SELECTOR,
  MESSEGIO_UNDELETO_ID_NAME,
  TAB_ELEMENT_SELECTOR,
} from './constants'
import { v4 } from 'uuid'

export const getMessageElements = (gridElement: HTMLElement): HTMLElement[] =>
  Array.from(getAll(MESSAGE_ELEMENT_SELECTOR, gridElement)) as HTMLElement[]

export const getGridElement = (tabElement: HTMLElement) =>
  get(GRID_ELEMENT_SELECTOR, tabElement) as HTMLElement

export const idFromElement = (messageElement: HTMLElement) =>
  messageElement.dataset[MESSEGIO_UNDELETO_ID_NAME]

export const createGridObserver =
  (db: DbController) =>
  (gridElement: HTMLElement): MutationObserver =>
    new MutationObserver(() => {
      const messageElements = getMessageElements(gridElement)

      db.removeTrash(messageElements.map(idFromElement))

      messageElements.forEach(db.addOrUpdateMessage)
    })

export const get = (
  selector: string,
  parentElement?: HTMLElement
): HTMLElement =>
  (parentElement || document).querySelector(selector) || document.body

export const getAll = (
  selector: string,
  parentElement?: HTMLElement
): HTMLElement[] =>
  pipe<[string], NodeListOf<HTMLElement>, HTMLElement[]>(
    querySelectorAllWithParent(parentElement),
    ifElse(lengthGreaterThan0, nodeListToArray, always([]))
  )(selector)

export const lengthGreaterThan = (num: number) => (el: WithLength) =>
  el.length > num

export const lengthGreaterThan0 = lengthGreaterThan(0)

export const nodeListToArray = (list: NodeListOf<HTMLElement>) =>
  Array.from(list)

export const querySelectorAllWithParent =
  (parentElement?: HTMLElement) =>
  (selector: string): NodeListOf<HTMLElement> =>
    (parentElement || document).querySelectorAll(selector)

export const getTabElements = (): HTMLElement[] => getAll(TAB_ELEMENT_SELECTOR)

export const filterTaggedTabElementsOut = filter(
  (tabElement: HTMLElement) => !tabElement.dataset[MESSEGIO_UNDELETO_ID_NAME]
)

export const tagTabElement = map((tabElement: HTMLElement) => {
  tabElement.dataset[MESSEGIO_UNDELETO_ID_NAME] = v4()
  return tabElement
})
