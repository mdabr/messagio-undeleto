import { createMessage } from './message'

const createMessageElement = (message: string) => {
  const messageElement = document.createElement('div')
  messageElement.innerHTML = message
  document.body.appendChild(messageElement)

  return messageElement
}

describe('message', () => {
  let body: string = null

  beforeEach(() => {
    body = document.body.innerHTML
  })

  afterEach(() => {
    document.body.innerHTML = body
  })

  describe('setText', () => {
    it('updates text kept in message object', () => {
      const message = createMessage(createMessageElement('test'))

      message.setText('test2')

      expect(message.getText()).toEqual('test2')
    })
  })

  describe('getText', () => {
    it('returns text currently kept in message object', () => {
      const message = createMessage(createMessageElement('test'))

      expect(message.getText()).toEqual('test')
    })
  })

  describe('getElement', () => {
    it('returns element currently kept in message object', () => {
      const messageElement = createMessageElement('test')
      const message = createMessage(messageElement)

      expect(message.getElement()).toEqual(messageElement)
    })
  })

  describe('getId', () => {
    it("returns message objects' id", () => {
      const message = createMessage(createMessageElement('test'))

      expect(message.getId()).toEqual(expect.any(String))
    })
  })
})
