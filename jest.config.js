/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  collectCoverageFrom: [
    '**/*.{js,ts}'
  ],
  roots: [
    '<rootDir>/src/'
  ],
  coveragePathIgnorePatterns: [
    'src/index.ts'
  ]
}
